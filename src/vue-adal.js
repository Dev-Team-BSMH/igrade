import Vue from "vue";
//import axios from "axios";
import {
  default as Adal,
  //AxiosAuthHttp,
  AuthenticationContext
} from "vue-adal";
import router from "./router";

Vue.use(Adal, {
  config: {
    tenant: "c98fb553-0659-4746-aefd-c234de2a5cfc",
    clientId: "708b672d-6703-4449-911a-426170af3a07",
    redirectUri: "http://localhost:8080",
    cacheLocation: "localStorage"
  },
  requireAuthOnInitialize: true,
  router: router
});

// Vue.use({
//   install (vue, opts = {}) {
//     vue.prototype.$graphApi = AxiosAuthHttp.createNewClient({
//       axios: axios,
//       resourceId: graphApiResource, // Resource id to get a token against
//       // Optional Params
//       router: router,
//       baseUrl: graphApiBase, // Base url to configure the client with
//
//       onTokenSuccess (http, context, token) { // Token success hook
//         // When an attempt to retrieve a token is successful, this will get called.
//         // This enables modification of the client after a successful call.
//         if (context.user) {
//           // Setup the client to talk with the Microsoft Graph API
//           http.defaults.baseURL = `${graphApiBase}/${context.user.profile.tid}`
//         }
//       },
//
//       onTokenFailure (error) { // Token failure hook
//         // When an attempt to retrieve a token is not successful, this will get called.
//         console.log(error)
//       }
//     })
//   }
// })

// console.log(AuthenticationContext.user.profile);
