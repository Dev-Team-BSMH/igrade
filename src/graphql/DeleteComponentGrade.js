import gql from "graphql-tag";

const DeleteComponentGrade = gql`
  mutation delete_grades_for_student($componentId: Int, $studentId: Int) {
    delete_grades_grades(
      where: {
        component_id: { _eq: $componentId }
        student_id: { _eq: $studentId }
      }
    ) {
      affected_rows
    }
  }
`;
export default DeleteComponentGrade;
