import gql from "graphql-tag";

const AllStudentsForGradesSheet = gql`
  query($cycleId: Int) {
    AllStudents: users_users(
      where: { students: { department: { cycle_id: { _eq: $cycleId } } } }
    ) {
      id
      first_name
      last_name
    }
  }
`;

export default AllStudentsForGradesSheet;
