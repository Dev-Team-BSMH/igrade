import gql from "graphql-tag";

const EvaluationTreeTestByCycle = gql`
  query($testId: Int, $cycleId: Int) {
    EditingScreenCurrentNode: grades_cycle_test_instances(
      where: { test_id: { _eq: $testId }, cycle_id: { _eq: $cycleId } }
    ) {
      order
      weight
      node: test {
        id
        name
        grades_aggregate {
          aggregate {
            count
          }
        }
        children: components {
          cycle_instances(
            order_by: { order: asc }
            where: { cycle_id: { _eq: $cycleId } }
          ) {
            order
            weight
            child: component {
              id
              name
            }
          }
        }
      }
    }
  }
`;
export default EvaluationTreeTestByCycle;
