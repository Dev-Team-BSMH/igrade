import gql from "graphql-tag";
const InsertAllGradesForTest = gql`
  mutation delete_grades_for_student(
    $gradeIDs: [Int]
    $gradesToInsert: [grades_grades_insert_input!]!
  ) {
    insert_grades_grades(
      objects: $gradesToInsert
      on_conflict: { constraint: student_and_component, update_columns: grade }
    ) {
      affected_rows
    }
    delete_grades_grades(where: { id: { _in: $gradeIDs } }) {
      affected_rows
    }
  }
`;
export default InsertAllGradesForTest;
