import gql from "graphql-tag";

const DeleteTest = gql`
  mutation delete_test($testId: Int, $cycleId: Int) {
    delete_grades_tests(
      where: {
        id: { _eq: $testId }
        cycle_instances: { cycle_id: { _eq: $cycleId } }
      }
    ) {
      affected_rows
    }
  }
`;
export default DeleteTest;
