import gql from "graphql-tag";

const AddComponent = gql`
  mutation update_componet_weight_and_name(
    $name: String
    $order: smallint
    $weight: Int
    $cycleId: Int
    $testId: Int
  ) {
    insert_grades_components(
      objects: [
        {
          name: $name
          test_id: $testId
          cycle_instances: {
            data: { cycle_id: $cycleId, weight: $weight, order: $order }
            on_conflict: {
              constraint: component_and_cycle_unique
              update_columns: [weight]
            }
          }
        }
      ]
      on_conflict: { constraint: components_pkey, update_columns: [name] }
    ) {
      affected_rows
    }
  }
`;
export default AddComponent;
