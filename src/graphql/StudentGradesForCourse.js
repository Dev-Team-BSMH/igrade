import gql from "graphql-tag";
const StudentGradesForCourse = gql`
  query($cycleId: Int, $courseId: Int) {
    grades: grades_student_course_grades(
      where: {
        student: { department: { cycle_id: { _eq: $cycleId } } }
        course_id: { _eq: $courseId }
      }
    ) {
      student {
        student_number
        user {
          first_name
          last_name
        }
        department {
          number
        }
      }
      grade
    }
  }
`;
export default StudentGradesForCourse;
