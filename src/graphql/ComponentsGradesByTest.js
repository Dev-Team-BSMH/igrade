import gql from "graphql-tag";

const ComponentsGradesByTest = gql`
  query($testId: Int, $cycleId: Int) {
    components_grades_by_test: users_students(
      where: {
        department: { cycle_id: { _eq: $cycleId } }
        components_grades: { id: { _is_null: false } }
      }
      order_by: { student_number: asc }
    ) {
      id
      student_number
      components_grades(where: { component: { test_id: { _eq: $testId } } }) {
        id
        grade
        component_id
        component {
          cycle_instances(where: { cycle_id: { _eq: $cycleId } }) {
            order
          }
        }
      }
    }
  }
`;

export default ComponentsGradesByTest;
