import gql from "graphql-tag";
const StudentGradesForTest = gql`
  query($cycleId: Int, $testId: Int) {
    grades: grades_student_test_grades(
      where: {
        student: { department: { cycle_id: { _eq: $cycleId } } }
        tests_id: { _eq: $testId }
      }
    ) {
      student {
        student_number
        user {
          first_name
          last_name
        }
        department {
          number
        }
      }
      grade
    }
  }
`;
export default StudentGradesForTest;
