import gql from "graphql-tag";

const SubjectsMaxOrder = gql`
  query($cycleId: Int) {
    subjectsMaxOrder: grades_cycle_subject_instances_aggregate(
      where: { cycle_id: { _eq: $cycleId } }
    ) {
      aggregate {
        max {
          order
        }
      }
    }
  }
`;
export default SubjectsMaxOrder;
