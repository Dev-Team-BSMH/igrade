import gql from "graphql-tag";

const EvaluationTreeComponentByCycle = gql`
  query($componentId: Int, $cycleId: Int) {
    EditingScreenCurrentNode: grades_cycle_component_instances(
      where: {
        component_id: { _eq: $componentId }
        cycle_id: { _eq: $cycleId }
      }
    ) {
      order
      weight
      node: component {
        id
        name
        grades_aggregate {
          aggregate {
            count
          }
        }
      }
    }
  }
`;
export default EvaluationTreeComponentByCycle;
