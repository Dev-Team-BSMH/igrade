import gql from "graphql-tag";

const SheetGradeForStudent = gql`
  query($students: [Int], $cycleId: Int) {
    users_students(
      where: {
        id: { _in: $students }
        _and: { courses_grades: { grade: { _is_null: false } } }
      }
    ) {
      id
      user {
        first_name
        last_name
      }
      courses: courses_grades {
        grade
        course {
          name
        }
      }
      subjects: subjects_grades {
        subject_id
        grade
        subject {
          name
          course_id
          cycle_instances(where: { cycle_id: { _eq: $cycleId } }) {
            order
          }
        }
      }
      tests: tests_grades {
        tests_id
        grade
        test {
          name
          id
          subject_id
          cycle_instances(where: { cycle_id: { _eq: $cycleId } }) {
            order
          }
        }
      }
      components: components_grades {
        component_id
        grade
        component {
          name
          id
          test_id
          cycle_instances(where: { cycle_id: { _eq: $cycleId } }) {
            order
          }
        }
      }
    }
  }
`;
export default SheetGradeForStudent;
