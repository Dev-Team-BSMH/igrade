import gql from "graphql-tag";

const EvaluationTreeByCycle = gql`
  query($cycleId: Int) {
    subjects: grades_cycle_subject_instances(
      order_by: { order: asc }
      where: { cycle_id: { _eq: $cycleId } }
    ) {
      order
      subject {
        id
        name
        tests(where: { cycle_instances: { id: { _is_null: false } } }) {
          cycle_instances(
            order_by: { order: asc }
            where: { test: { id: { _is_null: false } } }
          ) {
            order
            test {
              id
              name
              components(
                where: { cycle_instances: { id: { _is_null: false } } }
              ) {
                cycle_instances(
                  order_by: { order: asc }
                  where: { component: { id: { _is_null: false } } }
                ) {
                  order
                  component {
                    id
                    name
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

export default EvaluationTreeByCycle;
