import gql from "graphql-tag";

const ComponentsMaxOrder = gql`
  query($cycleId: Int, $testId: Int) {
    componentsMaxOrder: grades_cycle_component_instances_aggregate(
      where: {
        cycle_id: { _eq: $cycleId }
        _and: { component: { test_id: { _eq: $testId } } }
      }
    ) {
      aggregate {
        max {
          order
        }
      }
    }
  }
`;
export default ComponentsMaxOrder;
