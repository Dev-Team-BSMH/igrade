import gql from "graphql-tag";
const StudentGradesForComponent = gql`
  query($cycleId: Int, $componentId: Int) {
    grades: grades_grades(
      where: {
        student: { department: { cycle_id: { _eq: $cycleId } } }
        component_id: { _eq: $componentId }
      }
    ) {
      student {
        id
        student_number
        user {
          first_name
          last_name
        }
        department {
          number
        }
      }
      grade
    }
  }
`;
export default StudentGradesForComponent;
