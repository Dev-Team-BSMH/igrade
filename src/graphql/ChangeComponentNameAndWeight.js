import gql from "graphql-tag";
const ChangeComponentNameAndWeight = gql`
  mutation update_componet_weight_and_name(
    $name: String
    $order: smallint
    $componentId: Int
    $weight: Int
    $cycleId: Int
    $testId: Int
  ) {
    insert_grades_components(
      objects: [
        {
          name: $name
          id: $componentId
          test_id: $testId
          cycle_instances: {
            data: { cycle_id: $cycleId, weight: $weight, order: $order }
            on_conflict: {
              constraint: component_and_cycle_unique
              update_columns: [weight]
            }
          }
        }
      ]
      on_conflict: { constraint: components_pkey, update_columns: [name] }
    ) {
      affected_rows
    }
  }
`;
export default ChangeComponentNameAndWeight;
