import gql from "graphql-tag";
const AllDepartmentsByCycle = gql`
  query($cycleId: Int) {
    users_departments(where: { cycle_id: { _eq: $cycleId } }) {
      id
      number
    }
  }
`;
export default AllDepartmentsByCycle;
