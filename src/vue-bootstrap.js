import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "./assets/bootstrap.css";

Vue.use(BootstrapVue);
