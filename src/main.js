// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import BootstrapVue from "bootstrap-vue";
import axios from "axios";
import VueAxios from "vue-axios";
import { store } from "./store";
import { ClientTable } from "vue-tables-2";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "./vue-adal"; // FOR AAD AUTHENTICATION

import "vue-awesome/icons";
import Icon from "vue-awesome/components/Icon";

import { createProvider } from "./vue-apollo";
import VueSweetalert2 from "vue-sweetalert2";

const theme = {
  Minty: () => require("./assets/css/bootswatchMinty.min.css"),
  Flatly: () => require("./assets/css/bootswatchFlatly.min.css"),
  Journal: () => require("./assets/css/bootswatchJournal.min.css"),
  Sketchy: () => require("./assets/css/bootswatchSketchy.css"),
  Materia: () => require("./assets/css/bootswatchMateria.min.css"),
  Lux: () => require("./assets/css/bootswatchLux.min.css")
};

localStorage.theme
  ? theme[localStorage.theme]()
  : require("./assets/css/bootswatchFlatly.min.css");

Vue.use(VueSweetalert2);
Vue.use(BootstrapVue);
Vue.component("icon", Icon);

Vue.use(ClientTable);
Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  store: store,
  el: "#app",
  router,
  components: { App },
  apolloProvider: createProvider(),
  template: "<App/>"
});
