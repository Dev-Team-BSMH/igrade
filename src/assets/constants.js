export const insertingGradesTable = {
  SOLDIER_ID: 2,
  SOLDIER_NUMBER: 1,
  GRADE: 0
};
export const mutationConstants = {
  COMPONENT_VALUES: ["grade", "componentId", "studentId", "gradeId"]
};
export const tableDataConstants = {
  SOLDIER_ID: 1
};
export const insertingGradesTableTitles = {
  SOLDIER_ID: "מזהה",
  SOLDIER_NUMBER: "מספר חניך",
  GRADE: "ציון"
};
export const tableDataChangeConstants = {
  ROW: 0,
  COL: 1,
  AFTER_CHANGE_VALUE: 3
};

export const sweetAlert = {
  CONFIRM_BUTTON_TEXT: "אישור",
  CANCEL_BUTTON_TEXT: "ביטול",
  GENERAL_DELETE_TITLE: "אתה עומד למחוק ",
  TITLE_IF_GRADES:
    " אשר קיימים עבורו ציונים, מחיקה תמחק גם את הציונים השמורים בתוכו ",
  TITLE_IF_NOT_GRADES: " האם אתה בטוח ? ",
  TEXT_IF_GRADES: " בכדי למחוק הכנס את שם הנושא:  ",
  TEXT_IF_NO_VALUES: "נראה ששכחתם שם או משקל",
  TEXT_IF_DELETED_COMPONENT_GRADE: ".הציון נמחק בהצלחה",
  TEXT_COMPONENT_GRADE_DELETION_PROMPT_TITLE: "אתם עומדים למחוק ציון ל",
  TEXT_COMPONENT_GRADE_DELETION_PROMPT_HTML_BODY:
    "<span style='color:var(--danger)'>שים לב! </span>" +
    "לא ניתן לשחזר ציון לאחר מחיקה",
  TEXT_IF_WRONG_NAME: "הוכנס שם לא נכון"
};
export const deleteLevel = {
  SUBJECT: "נושא",
  TEST: "מבחן",
  COMPONENT: "רכיב"
};

export const EXCELLENTGRADE = 95;

export const theme = {
  FLATLY: "Flatly",
  MINTY: "Minty",
  JOURNAL: "Journal",
  SKETCHY: "Sketchy",
  MATERIA: "Materia",
  LUX: "Lux"
};
export const deleteWarning = {
  MESSEGE: "השארת ציון ריק תמחק את הציון עבור אותו חניך"
};
