
# ציונים

## רקע
המערכת תנהל את עץ ההערכה וכן את כל נתוני ההערכה הכמותיים.


## משתמשים ותפקידים
* מדריך/מ"מ
* צפ"ה/מק"ס


## תכולות

#### עץ הערכה 

<details dir="auto">
<summary>פירוט</summary>

* עץ ההערכה קורסי יורכב מ: `מקצוע` > `מבחן` > `מועד` > `קריטריון`.
* לכל אחד מהרכיבים יהיה משקל שייצג את החלקו היחסי ברכיב שמעליו.
* לכל מחזור יהיה עץ הערכה הניתן לעריכה ללא השפעה על המחזורים שלפניו.
* עבור כל רכיב יהיה ניתן להתאים משקל לחניך ספציפי.
* כל `מקצוע` יחזיק ציון שיהווה את רף מעבר המקצוע


###### הרשאות:
* מדריך/מ"מ - קריאה
* צפ"ה/מק"ס - עריכה

</details>


#### הזנת ציונים

<details dir="auto">
<summary>פירוט</summary>

* ממשק הזנת הציונים הראשי במערכת יהיה דמוי Excel.
* הציונים ינתנו פר `קריטריון` ויחושבו אוטומטית בעץ כלפי מעלה.
* קריטריון שלא יוזן לו ציון לא יחושב בהיררכיה (על מנת שיחשב כ-0 צריך להזין ציון 0).


###### הרשאות:
* צפ"ה/מק"ס - עריכה
* מ"מ/מדריך - עריכה

</details>


#### צפיה בציונים

<details dir="auto">
<summary>פירוט</summary>

* צפיה בציונים תיהיה על בסיס דוחות מוגדרים מראש.
* יהיה ניתן להתאים את הדוחות בזמן צפיה.

###### הרשאות:
* צפ"ה/מק"ס - עריכה
* מ"מ/מדריך - עריכה

</details>


#### בניית דוחות

<details dir="auto">
<summary>פירוט</summary>

* המערכת תאפשר בניית דוחות.
* בנית דוח תיהיה מורכבת משלושה חלקים:
  * בחירת שדות הדוח.
  * מיון / סינון הדוח.
  * בחירת דרך הצגת הדוח (טבלה או גרפים נבחרים).
* יהיה ניתן לשמור תבניות דו"ח לשימוש מהיר על פני המחזורים השונים.
* יהיה סט דוחות לשימוש יחידתי ובתחזוקת הצפ"ה

###### הרשאות:
* צפ"ה/מק"ס - עריכה
* מ"מ/מדריך - קריאה

</details>


#### הפקת גליון ציונים

<details dir="auto">
<summary>פירוט</summary>

* גליון הציונים יהיה מסמך על פי פורמט שיהיה ניתן להזין למערכת.
* יהיה ניתן להפיק גליונות ציונים לכל החניכים במחזור או לחניך ספציפי.


###### הרשאות:
* צפ"ה/מק"ס - עריכה
* מ"מ/מדריך - קריאה

</details>


## ממשקים

#### מסך הצגה ועריכת עץ ציונים
* אפשרות לעריכת עץ הציונים: הוספת מבחן, מקצוע וקריטריון ושינוי המשקל וציון הרף של כל אחד מהם.
![image](screens/tree.png)

#### מסך ציונים
* הזנת ציונים (מבוסס Excel).
* בחירת דוח וצפייה בציונים על בסיס טבלת נתונים.
![image](screens/grades.png)

#### מסך בניית דוחות
* במסך זה יבנו תבניות דוח לשימוש חוזר בין המחזורים.

#### מסך הפקת גיליונות ציונים
* לאחר בחירה בחניך ספציפי גיליון הציונים המלא ייוצא לקובץ word



## סכימה
```mermaid
graph TB
  subgraph Grades
    grade(grade)
    student(student)
    component(component)
    component_default_weight(component_default_weight)
    component_cycle_weight(component_cycle_weight)
    subject(subject)
    subject_default_weight(subject_default_weight)
    subject_cycle_weight(subject_cycle_weight)
    test(test)
    test_default_weight(test_default_weight)
    test_cycle_weight(test_cycle_weight)
    course(course)
    cycle(cycle)

    grade --> student
    grade --> component
    component_default_weight --> component
    component_cycle_weight --> component
    component_cycle_weight --> cycle
    component --> test
    test_default_weight --> test
    test_cycle_weight --> test
    test_cycle_weight --> cycle
    test --> subject
    subject --> course
    subject_default_weight --> subject
    subject_cycle_weight --> subject
    subject_cycle_weight --> cycle
  end
```



## ארכיטקטורה

#### רכיבים
```mermaid
graph RL
    GraphQL--- Apollo
    Apollo --- VueJS
```


#### הזדהות
```mermaid
sequenceDiagram
    participant App as App
    participant AAD as Azure Active Directory
    participant API as Ocean API

    App ->> AAD : get JWT
    AAD -->> App : return JWT
    App ->> API : fetch data with JWT header
    API -->> App : if authorized return data
```



## טכנולוגיות
* UI Client - [Vue.js](https://vuejs.org/)
* API Client - [Apollo](https://www.apollographql.com/)
* API - [GraphQL](https://graphql.org/)
* Authentication - [Azure Active Directory](https://docs.microsoft.com/en-us/azure/active-directory/) + [JWT](https://jwt.io/)